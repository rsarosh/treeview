﻿var data = [
    {
        n: "Asia",
        c: [
            { n: "Pakistan" },
            { n: "Iran" },
            { n: "Iraq" },
            { n: "China" },
            { n: "Japan" },
            { n: "Malaysia" },
            { n: "North Korea" },
            { n: "Saudi Arabia" },
            { n: "Turkmenistan" },
            { n: "Azerbeijan" }
        ],
        population: "Population: 4,164,252,000",
        countries: "Countries: 49",
        largestCities: "Largest cities: Tokyo (Japan), Jakarta (Indonesia), Seoul (South Korea)"
    }, {
        n: "Europe",
        c: [
            { n: "Italy", loadable: true },
            { n: "France", loadable: true, disabled: true },
            { n: "Germany", loadable: false },
            { n: "UK", loadable: 0 },
            { n: "Romania" },
            {
                n: "Turkey",
                c: [
                    { n: "Istanbul" },
                    { n: "Ankara" },
                    { n: "Izmir" }
                ]
            }
        ],
        population: "Population: 742,452,000",
        countries: "Countries: 50",
        largestCities: "Largest cities: Moscow (Russia), Istanbul (Turkey), Paris (France)"
    }, {
        n: "North America",
        c: [
            { n: "Canada" },
            { n: "US" },
            { n: "Mexico" }
        ],
        population: "Population: 565,265,000",
        countries: "Countries: 23",
        largestCities: "Largest cities: Mexico City (Mexico), New York City (USA), Los Angeles (USA)"
    }, {
        n: "South America",
        c: [
            {
                n: "Brazil",
                c: [
                    { n: "Sao Paolo" },
                    { n: "Rio de Janeiro" },
                    { n: "Salvador" }
                ]
            },
            { n: "Argentina" },
            { n: "Uruguay" },
            { n: "Paraguay" },
            { n: "Chile" },
            { n: "Colombia" },
            { n: "Venezuala" }
        ],
        population: "Population: 385,742,554",
        countries: "Countries: 12",
        largestCities: "Largest cities: Sao Paulo (Brazil), Buenos Aires (Argentina), Rio de Janeiro (Brazil)"
    }, {
        n: "Africa",
        c: [
            { n: "Egypt" },
            { n: "Kenya" },
            { n: "Algeria" },
            { n: "Tanzania" },
            { n: "Zambia" },
            { n: "Zimbabwe" }],
        population: "Population: 1,100,000,000",
        countries: "Countries: 54",
        largestCities: "Largest cities: Lagos (Nigeria), Cairo (Egypt), Kinshasa-Brazzaville (Congo)"
    }
], usStates = [
    { n: "Colorado" },
    { n: "Utah" },
    { n: "Washington" },
    { n: "Alabama" },
    {
        n: "North Carolina",
        c: [
            { n: "Raleigh" },
            { n: "Cary" },
            { n: "Durham" },
            { n: "Morrisville" }
        ]
    },
    { n: "Virginia" },
    { n: "South Carolina" },
    { n: "Minnesota" }
];
/// <reference path="scripts/typings/jquery/jquery.d.ts" />
/// <reference path="data.ts" />
var RMTTree;
(function (RMTTree) {
    var global = window, $ = jQuery;

    var Tree = (function () {
        function Tree(data) {
            var el = document.getElementById('content');
            data.forEach(function (value, index, arr) {
                if (value["n"])
                    $('content').add("ul", value.n);
            });
        }
        return Tree;
    })();
    RMTTree.Tree = Tree;
})(RMTTree || (RMTTree = {}));
/// <reference path="data.ts" />
/// <reference path="tree.ts" />
var rmtTree = RMTTree.Tree;

window.onload = function () {
    var tree = new rmtTree(data);
};
//# sourceMappingURL=compiled.js.map
