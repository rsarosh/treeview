﻿/// <reference path="scripts/typings/jquery/jquery.d.ts" />
/// <reference path="tree.ts" />

var global = window,
    $ = jQuery;


module data {

    export class Node implements Tree.INode {
        private _parent: Tree.INode;
        private _value: any;
        private _children: Tree.INode[];
        private _icon: string;
        private _expanded: boolean;

        constructor(parent: Tree.INode, value: any, expanded: boolean, children: Tree.INode[]) {
            this._parent = parent;
            this._value = value;
            this._children = children;
            this._expanded = expanded;

            if (this._expanded === undefined)
                this._expanded = false;

            if (this._expanded)
                this._icon = "\\images\\minus.png";
            else
                this._icon = "\\images\\plus.png";
        }

        get isExpanded(): boolean {
            return this._expanded;
        }

        set isExpanded(expanded: boolean) {
            this._expanded = expanded;
        }

        get nodeValue(): string {
            return this._value;
        }

        set nodeValue(value: string) {
            this._value = value;
        }

        get children(): Tree.INode[] {
            return this._children;
        }

        set children(children: Tree.INode[]) {
            this._children = children;
        }

        items(): Tree.INode[] {
            return this._children;
        }

        get icon(): string {
            return this._icon;
        }

        set icon(iconPath: string) {
            this._icon = iconPath;
        }

        get parent(): Tree.INode {
            return this._parent;
        }

        set parent(value: Tree.INode) {
            this._parent = value;
        }
    }

    export class Data implements Tree.IData {

          AddChild = (parent: Tree.INode, expanded: boolean, node: Tree.INode): Tree.INode[]=> {
            if (expanded === false)
                return null;
            var nodes = [];
            for (var i = 0; i < 200; i++) {
                var n = new Node(parent, parent.nodeValue + 'Child-' + i + '', false, null);
                //no need to call children as childrens are never filled initially
                //n.children = this.AddChild(n, false, node) //must keep expanded false, to block it from loading children
            nodes.push(n);
            }
            return nodes;
        }

        TreeData = (): Tree.INode[] => {
            var nodes = [];
            var level = 1;
            for (var i = 0; i < 10; i++) {
                var n = new Node(null, 'Root' + i, false, null);
                nodes.push(n);
            }
            return nodes;
        }
    }
}
