/// <reference path="scripts/typings/knockout/knockout.d.ts" />
var TreeTempate;
(function (TreeTempate) {
    var stringTemplate = (function () {
        function stringTemplate(templateName, templates) {
            this._templateName = templateName;
            this._templates = templates;
        }
        //data (key, value) {
        //    this._templates._data = this._templates._data || {};
        //    this._templates._data[this._templateName] = this._templates._data[this._templateName] || {};
        //    if (arguments.length === 1) {
        //        return this._templates._data[this._templateName][key];
        //    }
        //    this._templates._data[this._templateName][key] = value;
        //}
        /* nativeTemplateEngine renderTempateSource calls this text
         * https://github.com/knockout/knockout/blob/master/src/templating/native/nativeTemplateEngine.js
         */
        stringTemplate.prototype.text = function (value) {
            if (arguments.length === 0) {
                return this._templates[this._templateName];
            }
            this[this._templateName] = value;
        };
        return stringTemplate;
    })();
    ;
    //modify an existing templateEngine to work with string templates
    function createStringTemplateEngine(templateEngine, templates) {
        /* override makeTemplateSource. as per templateSource.js of KO. This method will be called by templateEngine.renderTemplate
         * When template engine starts rendering template. renderTemplate calls makeTemplateSource, which returns templateSource. templateSource text
         * method is called to get the template
         */
        templateEngine.makeTemplateSource = function (templateName) {
            return new stringTemplate(templateName, templates);
        };
        return templateEngine;
    }
    TreeTempate.createStringTemplateEngine = createStringTemplateEngine;
    ;
})(TreeTempate || (TreeTempate = {}));
;
/// <reference path="scripts/typings/jquery/jquery.d.ts" />
/// <reference path="templateengine.ts" />
///// <reference path="data.ts" />
var Tree;
(function (Tree) {
    var viewModel = (function () {
        function viewModel(datanode) {
            var _this = this;
            this._childrenFound = false;
            this.templates = {
                tree: "<ul><!-- ko template: { name: 'nodes', foreach: _items } --><!-- /ko --></ul>",
                subnodes: " <!-- ko template: { name: 'nodes', foreach: $data } --><!-- /ko --> ",
                nodes: "<!-- ko template: { name: 'nodeCore', data: $data  } --> <!-- /ko -->",
                nodeCore: "<li data-bind='attr: {\"class\" : \"tab-content show\"}'>" + "<!-- ko template: { name: 'nodeContent', data: $data  } --> <!-- /ko -->" + " <!-- ko template: { name: 'subnodes', data: $data.children } --> <!-- /ko-->" + "</li>",
                nodeContent: "<div><img height='25' width='25' data-bind= 'click: $root.nodeClicked, attr: {src:$data.icon}' /> <div style='margin-top: -28px; margin-left:20px;' data-bind='html: $data.nodeValue'></div></div> "
            };
            this.loadData = function (expanded, node) {
                _this._items(_this._data.TreeData());
            };
            //Use lambda syntax to keep 'this' intact
            this.nodeClicked = function (selectedNode, event) {
                selectedNode.isExpanded = !selectedNode.isExpanded;
                if (!selectedNode.isExpanded) {
                    selectedNode.children = null;
                    selectedNode.icon = "\\images\\plus.png";
                    var arr = ko.toJS(_this._items);
                    //Now splice the orginal Data here to fill only the needed items.
                    _this._items(null);
                    _this._items(arr);
                    return;
                }
                _this._childrenFound = false;
                var arr = ko.toJS(_this._items);
                var index = -1;
                if (_this.deepSearch(selectedNode, arr) != -1) {
                    //Children were not found, no need to change the icon
                    if (_this._childrenFound) {
                        if (selectedNode.isExpanded)
                            selectedNode.icon = "\\images\\minus.png";
                        else
                            selectedNode.icon = "\\images\\plus.png";
                    }
                    //Lets close the crazy leaves, as they crash the browsers and hang the system
                    if (_this._isChildNode2Big) {
                        var selectedNodes = Array();
                        var n = selectedNode;
                        while (n != null) {
                            selectedNodes.push(n);
                            n = n.parent;
                        }
                        arr = _this.autoCloseNodes(selectedNodes, arr);
                    }
                    _this._items(null);
                    _this._items(arr);
                }
            };
            this.autoCloseNodes = function (selectedNodes, arr) {
                arr.forEach(function (node, index, arr) {
                    if (!node.isExpanded)
                        return;
                    for (var i = 0; i < selectedNodes.length; i++) {
                        if (node.nodeValue === selectedNodes[i].nodeValue)
                            return;
                    }
                    if (node.children.length > _this._tooManychildren) {
                        node.children = null;
                        node.isExpanded = false;
                        node.icon = "\\images\\plus.png";
                    }
                    else {
                        node.children = _this.autoCloseNodes(selectedNodes, node.children);
                    }
                });
                return arr;
            };
            this.deepSearch = function (selectedNode, arr) {
                if (arr === undefined || arr === null)
                    return -1;
                var index = -1;
                index = _this.myIndexOf(selectedNode, arr);
                if (index === -1) {
                    for (var i = 0; i < arr.length; i++) {
                        index = _this.deepSearch(selectedNode, arr[i].children);
                        if (index != -1)
                            break;
                    }
                }
                else {
                    _this._isChildNode2Big = false;
                    selectedNode.children = _this._data.AddChild(selectedNode, selectedNode.isExpanded, selectedNode);
                    if (selectedNode.children.length >= _this._children2Big) {
                        _this._isChildNode2Big = true;
                    }
                    _this._childrenFound = (selectedNode.children != null);
                    arr[index] = selectedNode;
                    return index;
                }
                return index;
            };
            this._toggle = false;
            this._items = ko.observableArray([]);
            this.TreeTemplate = 'tree';
            this.SetEngine();
            this._data = datanode;
            this.loadData(false, null);
            this._tooManychildren = 100;
            this._children2Big = 100;
        }
        viewModel.prototype.myIndexOf = function (obj, arr) {
            for (var i = 0; i < arr.length; i++) {
                if (arr[i].nodeValue == obj.nodeValue) {
                    return i;
                }
            }
            return -1;
        };
        viewModel.prototype.SetEngine = function () {
            this.customTemplateEngine = TreeTempate.createStringTemplateEngine(new ko.nativeTemplateEngine(), this.templates);
            ko.setTemplateEngine(this.customTemplateEngine);
        };
        return viewModel;
    })();
    Tree.viewModel = viewModel;
    ;
})(Tree || (Tree = {}));
/// <reference path="scripts/typings/jquery/jquery.d.ts" />
/// <reference path="tree.ts" />
var global = window, $ = jQuery;
var data;
(function (data) {
    var Node = (function () {
        function Node(parent, value, expanded, children) {
            this._parent = parent;
            this._value = value;
            this._children = children;
            this._expanded = expanded;
            if (this._expanded === undefined)
                this._expanded = false;
            if (this._expanded)
                this._icon = "\\images\\minus.png";
            else
                this._icon = "\\images\\plus.png";
        }
        Object.defineProperty(Node.prototype, "isExpanded", {
            get: function () {
                return this._expanded;
            },
            set: function (expanded) {
                this._expanded = expanded;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Node.prototype, "nodeValue", {
            get: function () {
                return this._value;
            },
            set: function (value) {
                this._value = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Node.prototype, "children", {
            get: function () {
                return this._children;
            },
            set: function (children) {
                this._children = children;
            },
            enumerable: true,
            configurable: true
        });
        Node.prototype.items = function () {
            return this._children;
        };
        Object.defineProperty(Node.prototype, "icon", {
            get: function () {
                return this._icon;
            },
            set: function (iconPath) {
                this._icon = iconPath;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Node.prototype, "parent", {
            get: function () {
                return this._parent;
            },
            set: function (value) {
                this._parent = value;
            },
            enumerable: true,
            configurable: true
        });
        return Node;
    })();
    data.Node = Node;
    var Data = (function () {
        function Data() {
            this.AddChild = function (parent, expanded, node) {
                if (expanded === false)
                    return null;
                var nodes = [];
                for (var i = 0; i < 200; i++) {
                    var n = new Node(parent, parent.nodeValue + 'Child-' + i + '', false, null);
                    //no need to call children as childrens are never filled initially
                    //n.children = this.AddChild(n, false, node) //must keep expanded false, to block it from loading children
                    nodes.push(n);
                }
                return nodes;
            };
            this.TreeData = function () {
                var nodes = [];
                var level = 1;
                for (var i = 0; i < 10; i++) {
                    var n = new Node(null, 'Root' + i, false, null);
                    nodes.push(n);
                }
                return nodes;
            };
        }
        return Data;
    })();
    data.Data = Data;
})(data || (data = {}));
//# sourceMappingURL=_compile.js.map