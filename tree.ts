﻿/// <reference path="scripts/typings/jquery/jquery.d.ts" />
/// <reference path="templateengine.ts" />
///// <reference path="data.ts" />



module Tree {

    export interface INode {
        //constructor signature is not needed, but leaving it here, so the implementor
        //of this interface gets a hint how to construct Node
        //new (parent: INode, value: any, expanded: boolean, children: INode[]): any;
        isExpanded: boolean;
        nodeValue: string;
        children: INode[];
        icon: string;
        parent: INode;
    }

    export interface IData {
        AddChild (parent: INode, expanded: boolean, node: INode): INode[];
        TreeData(): INode[];
    }

   export class viewModel {
       private _toggle: boolean;
       private _items: any;
       public TreeTemplate: string;
       public customTemplateEngine: any;
       private _childrenFound = false;
       private _data: IData; 
       private _isChildNode2Big: boolean;
       private _tooManychildren: number;
       private _children2Big: number;
    
       constructor(datanode: IData) {
           this._toggle = false;
           this._items = ko.observableArray<Node>([]);
           this.TreeTemplate = 'tree';
           this.SetEngine();
           this._data = datanode;
           this.loadData(false, null);
           this._tooManychildren = 100;
           this._children2Big = 100;
       }

       public  templates = {
           tree: "<ul><!-- ko template: { name: 'nodes', foreach: _items } --><!-- /ko --></ul>",
            subnodes: " <!-- ko template: { name: 'nodes', foreach: $data } --><!-- /ko --> ",
            nodes: "<!-- ko template: { name: 'nodeCore', data: $data  } --> <!-- /ko -->",
            nodeCore:
            "<li data-bind='attr: {\"class\" : \"tab-content show\"}'>" +
                        "<!-- ko template: { name: 'nodeContent', data: $data  } --> <!-- /ko -->"
                + " <!-- ko template: { name: 'subnodes', data: $data.children } --> <!-- /ko-->"
            + "</li>"
           ,
           nodeContent: "<div><img height='25' width='25' data-bind= 'click: $root.nodeClicked, attr: {src:$data.icon}' /> <div style='margin-top: -28px; margin-left:20px;' data-bind='html: $data.nodeValue'></div></div> "
        };

        loadData = (expanded: boolean, node: INode) => {
            this._items(this._data.TreeData());
        }

        myIndexOf(obj: any, arr: any) {
            for (var i = 0; i < arr.length; i++) {
                if (arr[i].nodeValue == obj.nodeValue) {
                    return i;
                }
            }
            return -1;
        }

        //Use lambda syntax to keep 'this' intact
        nodeClicked = (selectedNode: INode, event: any) => {
            selectedNode.isExpanded = !selectedNode.isExpanded;

            if (!selectedNode.isExpanded) { //If tree is not expanded, lets throw the children away
                selectedNode.children = null;
                selectedNode.icon = "\\images\\plus.png";
                var arr = ko.toJS(this._items);
                //Now splice the orginal Data here to fill only the needed items.
                this._items(null);
                this._items(arr);
                return;
            }

            this._childrenFound = false;
            var arr = ko.toJS(this._items);
            var index = -1;
            if (this.deepSearch(selectedNode, arr) != -1) {
                //Children were not found, no need to change the icon
                if (this._childrenFound) {
                    if (selectedNode.isExpanded)
                        selectedNode.icon = "\\images\\minus.png";
                    else
                        selectedNode.icon = "\\images\\plus.png";
                }
                //Lets close the crazy leaves, as they crash the browsers and hang the system
                if (this._isChildNode2Big) {
                    var selectedNodes: INode[] = Array ();
                    var n = selectedNode;
                    while (n != null){
                        selectedNodes.push(n);
                        n = n.parent;
                    }
                    arr = this.autoCloseNodes(selectedNodes, arr);
                }
                this._items(null);
                this._items(arr);
            }
        };

    autoCloseNodes = (selectedNodes:INode[], arr: INode[]): INode[] => {
        arr.forEach((node, index, arr) => {
            if (!node.isExpanded)
                return;
            for (var i = 0; i < selectedNodes.length; i++) {
                if (node.nodeValue === selectedNodes[i].nodeValue)
                    return;
            }
            if (node.children.length > this._tooManychildren) {
                node.children = null;
                node.isExpanded = false;
                node.icon = "\\images\\plus.png";
            } else {
                node.children = this.autoCloseNodes (selectedNodes, node.children);
            }
        });
        return arr;
    }

    deepSearch = (selectedNode: INode, arr: any): number => {
            if (arr === undefined || arr === null)
                return -1;
            var index = -1;
            index = this.myIndexOf(selectedNode, arr);
            if (index === -1) {
                //lets search the children
                for (var i = 0; i < arr.length; i++){
                    index  = this.deepSearch(selectedNode, arr[i].children);
                    if (index != -1) break;
                    }
            } else {
                this._isChildNode2Big = false;
                selectedNode.children = this._data.AddChild(selectedNode, selectedNode.isExpanded, selectedNode);
                if (selectedNode.children.length >= this._children2Big) {
                    this._isChildNode2Big = true;
                }

                this._childrenFound = (selectedNode.children != null )
                arr[index] = selectedNode;
                return index;
            }
            return index;
        }

        SetEngine() {
            this.customTemplateEngine = TreeTempate.createStringTemplateEngine(new ko.nativeTemplateEngine(), this.templates);
            ko.setTemplateEngine(this.customTemplateEngine);
        }
        
   };

}


























