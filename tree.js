﻿/// <reference path="scripts/typings/jquery/jquery.d.ts" />
/// <reference path="data.ts" />
var RMTTree;
(function (RMTTree) {
    var global = window, $ = jQuery;

    var Tree = (function () {
        function Tree(data) {
            var el = document.getElementById('content');
            data.forEach(function (value, index, arr) {
                if (value["n"])
                    $('content').add("ul", value.n);
            });
        }
        return Tree;
    })();
    RMTTree.Tree = Tree;
})(RMTTree || (RMTTree = {}));
//# sourceMappingURL=tree.js.map
