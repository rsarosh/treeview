/// <reference path="Scripts/typings/knockout/knockout.d.ts" />
/// <reference path="Scripts/typings/jquery/jquery.d.ts" />
declare module TreeTempate {
    function createStringTemplateEngine(templateEngine: any, templates: any): any;
}
declare module Tree {
    interface INode {
        isExpanded: boolean;
        nodeValue: string;
        children: INode[];
        icon: string;
        parent: INode;
    }
    interface IData {
        AddChild(parent: INode, expanded: boolean, node: INode): INode[];
        TreeData(): INode[];
    }
    class viewModel {
        private _toggle;
        private _items;
        TreeTemplate: string;
        customTemplateEngine: any;
        private _childrenFound;
        private _data;
        private _isChildNode2Big;
        private _tooManychildren;
        private _children2Big;
        constructor(datanode: IData);
        templates: {
            tree: string;
            subnodes: string;
            nodes: string;
            nodeCore: string;
            nodeContent: string;
        };
        loadData: (expanded: boolean, node: INode) => void;
        myIndexOf(obj: any, arr: any): number;
        nodeClicked: (selectedNode: INode, event: any) => void;
        autoCloseNodes: (selectedNodes: INode[], arr: INode[]) => INode[];
        deepSearch: (selectedNode: INode, arr: any) => number;
        SetEngine(): void;
    }
}
declare var global: Window, $: JQueryStatic;
declare module data {
    class Node implements Tree.INode {
        private _parent;
        private _value;
        private _children;
        private _icon;
        private _expanded;
        constructor(parent: Tree.INode, value: any, expanded: boolean, children: Tree.INode[]);
        isExpanded: boolean;
        nodeValue: string;
        children: Tree.INode[];
        items(): Tree.INode[];
        icon: string;
        parent: Tree.INode;
    }
    class Data implements Tree.IData {
        AddChild: (parent: Tree.INode, expanded: boolean, node: Tree.INode) => Tree.INode[];
        TreeData: () => Tree.INode[];
    }
}
